#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
#include <cmath>


using namespace std;

char s[6][4]; //6xSciana=kostka
char savedS[6][4]; // zapisana kostka pózniej
char bufor1;
int liczba, liczba2;
int iloscMozliwosci, rozmiarWektora, brakujaceRuchy;
int solutionVector[15]; //maksymalnie 15 ruchow na rozwiazanie, jesli zero to konczy obracac i sprawdza np {1,5,7,6,2,4,0..0} -> U F2 L F' U2 F
bool komunikaty, setKomunikaty, setLiczby = false;


void saveCube()
{
    for(int i=0;i<6;i++)
    {
        for(int j=0;j<4;j++)
        {
            savedS[i][j]=s[i][j];
        }
    }
}

void loadCube()
{
    for(int i=0;i<6;i++)
    {
        for(int j=0;j<4;j++)
        {
            s[i][j]=savedS[i][j];
        }
    }
}

void init()
{
    srand(time(NULL));

    setKomunikaty = true;
    komunikaty = setKomunikaty;

    for(int i=0;i<=5;i++)
    {
        for(int j=0;j<=3;j++)
        {
            s[i][j] = i+1;
        }
    }
    /*
    s[0][0]='A';
    s[0][1]='B';
    s[0][2]='C';
    s[0][3]='D';

    s[1][0]='E';
    s[1][1]='F';
    s[1][2]='G';
    s[1][3]='H';

    s[2][0]='I';
    s[2][1]='J';
    s[2][2]='K';
    s[2][3]='L';

    s[3][0]='M';
    s[3][1]='N';
    s[3][2]='O';
    s[3][3]='P';

    s[4][0]='Q';
    s[4][1]='R';
    s[4][2]='S';
    s[4][3]='T';

    s[5][0]='U';
    s[5][1]='V';
    s[5][2]='W';
    s[5][3]='X';

    saveCube();
    */
}

void rotateWall(int wallN, int direction) //wallN: 0-góra, 1-dól, 2-front, 3-tyl, 4-lewo, 5-prawo;    direcion 1-jak zegar, 2-180st, 3-przeciwnie do zegara
{
    string sciana="";
    string strona="";
    char *wall = s[wallN];

    for(int i=0;i<direction;i++)
    {
    //sama sciana
    bufor1 = wall[3];
    wall[3]=wall[2];
    wall[2]=wall[1];
    wall[1]=wall[0];
    wall[0]=bufor1;

    //boki
    switch (wallN)
    {
    case 0:
        sciana="gorna";

        //pola lewe
        bufor1=s[4][0];
        s[4][0]=s[2][0];
        s[2][0]=s[5][0];
        s[5][0]=s[3][0];
        s[3][0]=bufor1;

        //pola prawe
        bufor1=s[4][1];
        s[4][1]=s[2][1];
        s[2][1]=s[5][1];
        s[5][1]=s[3][1];
        s[3][1]=bufor1;

        break;
    case 1:
        sciana="dolna";

        //pola lewe
        bufor1=s[4][3];
        s[4][3]=s[3][3];
        s[3][3]=s[5][3];
        s[5][3]=s[2][3];
        s[2][3]=bufor1;

        //pola prawe
        bufor1=s[4][2];
        s[4][2]=s[3][2];
        s[3][2]=s[5][2];
        s[5][2]=s[2][2];
        s[2][2]=bufor1;

        break;
    case 2:
        sciana="przednia";

        //pola lewe
        bufor1=s[4][2];
        s[4][2]=s[1][1];
        s[1][1]=s[5][0];
        s[5][0]=s[0][3];
        s[0][3]=bufor1;

        //pola prawe
        bufor1=s[4][1];
        s[4][1]=s[1][0];
        s[1][0]=s[5][3];
        s[5][3]=s[0][2];
        s[0][2]=bufor1;

        break;
    case 3:
        sciana="tylnia";

        //pola lewe
        bufor1=s[5][2];
        s[5][2]=s[1][3];
        s[1][3]=s[4][0];
        s[4][0]=s[0][1];
        s[0][1]=bufor1;

        //pola prawe
        bufor1=s[5][1];
        s[5][1]=s[1][2];
        s[1][2]=s[4][3];
        s[4][3]=s[0][0];
        s[0][0]=bufor1;


        break;
    case 4:
        sciana="lewa";

        //pola lewe
        bufor1=s[3][2];
        s[3][2]=s[1][0];
        s[1][0]=s[2][0];
        s[2][0]=s[0][0];
        s[0][0]=bufor1;

        //pola prawe
        bufor1=s[3][1];
        s[3][1]=s[1][3];
        s[1][3]=s[2][3];
        s[2][3]=s[0][3];
        s[0][3]=bufor1;

        break;
    case 5:
        sciana="prawa";

        //pola lewe
        bufor1=s[2][2];
        s[2][2]=s[1][2];
        s[1][2]=s[3][0];
        s[3][0]=s[0][2];
        s[0][2]=bufor1;

        //pola prawe
        bufor1=s[2][1];
        s[2][1]=s[1][1];
        s[1][1]=s[3][3];
        s[3][3]=s[0][1];
        s[0][1]=bufor1;

        break;
        }
    }

     switch (direction) {
    case 1:
        strona="w prawo";
        break;
    case 2:
        strona="o 180 stopni";
        break;
    case 3:
        strona="w lewo";
        break;
    }
    if (komunikaty)
    cout<<"Sciana "<<sciana<<" zostala obrocona "<<strona<<"."<<endl;
}

void drawCube()
{
    int p = 0;
    if(setLiczby) p = 47;
    cout<<"\n\t  "<<(char) (s[0][0]+p)<<(char) (s[0][1]+p)<<endl;
    cout<<"\t  "<<(char) (s[0][3]+p)<<(char) (s[0][2]+p)<<endl;
    cout<<"\t"<<(char) (s[4][0]+p)<<(char) (s[4][1]+p)<<(char) (s[2][0]+p)<<(char) (s[2][1]+p)<<(char) (s[5][0]+p)<<(char) (s[5][1]+p)<<(char) (s[3][0]+p)<<(char) (s[3][1]+p)<<endl;
    cout<<"\t"<<(char) (s[4][3]+p)<<(char) (s[4][2]+p)<<(char) (s[2][3]+p)<<(char) (s[2][2]+p)<<(char) (s[5][3]+p)<<(char) (s[5][2]+p)<<(char) (s[3][3]+p)<<(char) (s[3][2]+p)<<endl;
    cout<<"\t  "<<(char) (s[1][0]+p)<<(char) (s[1][1]+p)<<endl;
    cout<<"\t  "<<(char) (s[1][3]+p)<<(char) (s[1][2]+p)<<endl;
    /*
    cout<<"\n\t    "<<s[0][0]<<" "<<s[0][1]<<endl;
    cout<<"\t    "<<s[0][3]<<" "<<s[0][2]<<endl;
    cout<<"\t"<<s[4][0]<<" "<<s[4][1]<<" "<<s[2][0]<<" "<<s[2][1]<<" "<<s[5][0]<<" "<<s[5][1]<<" "<<s[3][0]<<" "<<s[3][1]<<endl;
    cout<<"\t"<<s[4][3]<<" "<<s[4][2]<<" "<<s[2][3]<<" "<<s[2][2]<<" "<<s[5][3]<<" "<<s[5][2]<<" "<<s[3][3]<<" "<<s[3][2]<<endl;
    cout<<"\t    "<<s[1][0]<<" "<<s[1][1]<<endl;
    cout<<"\t    "<<s[1][3]<<" "<<s[1][2]<<endl;
    */
}


void shufle(int times)
{
    for(int i=0;i<times;i++)
    {
        rotateWall(rand()%6,rand()%3+1);
        //drawCube();
    }
}

void doFastRotation(int numb) // wykonuje ktoras z 9 mozliwosci obrotow, pozostale obroty sa tozsame z ktoryms innym
{
    switch (numb)
    {
    case 1:
        rotateWall(0,1);
        break;
    case 2:
        rotateWall(0,2);
        break;
    case 3:
        rotateWall(0,3);
        break;
    case 4:
        rotateWall(2,1);
        break;
    case 5:
        rotateWall(2,2);
        break;
    case 6:
        rotateWall(2,3);
        break;
    case 7:
        rotateWall(5,1);
        break;
    case 8:
        rotateWall(5,2);
        break;
    case 9:
        rotateWall(5,3);
        break;
    default:
        break;
    }

}

bool cubeSolved()
{
    bool wynik = true;
    for(int i=0;i<=5;i++)
    {
        if(s[i][0]==s[i][1] && s[i][0]==s[i][2] && s[i][0]==s[i][3])
        {

            //if(komunikaty) cout<<"Sciana "<<i<<" jest dobrze :) ";
        }
        else
        {
            wynik = false;
        }
    }
    return wynik;
}

void wypiszRozwiazanie(bool sposob)
{
    cout<<"Rozwiazanie to: ";

    for (int i=0;i<rozmiarWektora;i++)
    {
        if(sposob)
        {
            switch (solutionVector[i])
            {
        case 1:
            cout<<"U ";
             break;
        case 2:
            cout<<"U2 ";
             break;
        case 3:
            cout<<"U' ";
             break;
        case 4:
            cout<<"F ";
             break;
        case 5:
            cout<<"F2 ";
             break;
        case 6:
            cout<<"F' ";
             break;
        case 7:
            cout<<"R ";
             break;
        case 8:
            cout<<"R2 ";
             break;
        case 9:
            cout<<"R' ";
            break;
            }
        }
        else
        {
            cout<<solutionVector[i]<<", ";
        }
    }

    cout<<endl;
}


void wykonajWektor()
{
    for(int i=0;i<rozmiarWektora;i++)
    {
        komunikaty = false;
        doFastRotation(solutionVector[i]);
        komunikaty = setKomunikaty;
    }
}



void recur(int nrRuchu)
{
    if( ! cubeSolved())
    {
        //cin.get();
        //cout<<"Wykonuje krok "<<rozmiarWektora-brakujaceRuchy<<" z "<<rozmiarWektora<<endl;
        //cout<<"Wrzucam "<<nrRuchu<<" do wektora rozwiazan."<<endl;
        solutionVector[rozmiarWektora-brakujaceRuchy] = nrRuchu;
        brakujaceRuchy -= 1;

        if(brakujaceRuchy==0)
        {
            //cout<<"Sprawdze czy wektor rozwiazania jest OK"<<endl;

            wykonajWektor();

            if(cubeSolved())
            {
                    cout<<"Kostka zlozona w "<<rozmiarWektora<<" ruchach"<<endl;
            }
            else
            {
                //cout<<"Jednak ten wektor nie byl dobrze..."<<endl;
                loadCube();
                brakujaceRuchy += 1;
            }
        }
        else
        {
            //cout<<"Zostalo jeszcze "<<brakujaceRuchy<<" ruchow."<<endl;

            int poprzedni = solutionVector[rozmiarWektora-brakujaceRuchy-1];
            if(poprzedni==1 || poprzedni==2 || poprzedni==3)
            {
                for(int i=4;i<=9;i++)
                {
                    recur(i);
                }
            }
            else if(poprzedni==4 || poprzedni==5 || poprzedni==6)
            {
                for(int i=7;i<=12;i++)
                {
                    recur(i%9);
                }
            }
            else if(poprzedni==7 || poprzedni==8 || poprzedni==9)
            {
                for(int i=1;i<=6;i++)
                {
                    recur(i);
                }
            }



            brakujaceRuchy += 1;
        }
    }
}


void solveCube()
{
    if(!cubeSolved())
    {
        saveCube();

        for(rozmiarWektora=1;rozmiarWektora<15;rozmiarWektora++) //rozmiar wektora ruchow
        {
            if(komunikaty) cout<<"\nProbuje rozwiazanie o dlugosci: "<<rozmiarWektora<<endl;
            brakujaceRuchy = rozmiarWektora;
            for(int k=1;k<=9;k++) recur(k);

            if(cubeSolved()) break;
        }

        cout<<"Kostka rozwiazana!"<<endl;
        wypiszRozwiazanie(1);

    }
    else
        cout<<"Kostka rozwiazana!"<<endl;
}

int main()
{
    init();
    cout<<cubeSolved()<<endl;
    cout <<"Kostka wyglada tak:"<< endl;
    drawCube();

    while(true) //petla glowna
    {
        cout<<endl<<"Co chcesz wykonac?\n1-Ukladanie kostki\n2-Pomieszanie\n3-Auto ulozenie\n4-Sprawdzenie \n5-Wpisz stany \n6-Settings \n7-Export kodu do Cosimira"<<endl;

        cin>>liczba;
        cout<<endl;

        switch(liczba)
        {
        case 1: //ukladanie kostki
            while(true)
            {
                cout<<"Podaj ktora sciane obrocic i o ile obrotow?\n0-Gora\n1-Dol\n2-Przod\n3-Tyl\n4-Lewa\n5-Prawa\n6-Wyjdz"<<endl;
                cout<<"\n1-Clockwise\n2-180 stopni\n3-Counter clockwise"<<endl;
                    if (liczba==6) break;
                cin>>liczba;
                if(liczba<6) cin>>liczba2;
                cout<<liczba;
                rotateWall(liczba,liczba2);
                drawCube();

            }
            break;
        case 2:
            cout<<"Ile obrotow mieszania? ";
            cin>>liczba;
            shufle(liczba);
            break;
        case 3:
            solveCube();
            break;
        case 4:
            cout<<cubeSolved();
            break;
        case 5:
            cout<<"Wpisuj stany kolejnych pol scian. !!!UWAGA!!! Moze powstac kostka nierozwiazywalna!"<<endl;
            string znaki[6];
            znaki = {"Gora","Dol","Przod","Tyl","Lewo","Prawo"};
            for(int i=0;i<6;i++)
            {
                cout<<znaki[i];
                for(int j=0;j<4;j++)
                {
                    cin>>s[i][j];
                }
            }
            drawCube();
            break;
        case 6:
            cout<<"1-Zalacz komunikaty \n2-Wylacz komunikaty \n3-Sciany jako obrazki \n4-Sciany jako cyfry"<<endl;
            cin>>liczba;
            switch(liczba)
            {
            case 1:
                setKomunikaty = true;
                break;
            case 2:
                setKomunikaty = false;
                break;
            case 3:
                setLiczby = false;
                break;
            case 4:
                setLiczby = true;
                break;
            default:
                break;
            }
            break;

        case 7:
            cout<< "69 REM GORNA"<<endl;
            cout<< "70 S(1,1)= "<<(int) s[0][0]<<endl;
            cout<< "80 S(1,2)= "<<(int) s[0][1]<<endl;
            cout<< "90 S(1,3)= "<<(int) s[0][2]<<endl;
            cout<< "100 S(1,4)= "<<(int) s[0][3]<<endl<<endl;

            cout<< "109 REM DOLNA"<<endl;
            cout<< "110 S(2,1)= "<<(int) s[1][0]<<endl;
            cout<< "120 S(2,2)= "<<(int) s[1][1]<<endl;
            cout<< "130 S(2,3)= "<<(int) s[1][2]<<endl;
            cout<< "140 S(2,4)= "<<(int) s[1][3]<<endl<<endl;

            cout<< "149 REM PRZEDNIA"<<endl;
            cout<< "150 S(3,1)= "<<(int) s[2][0]<<endl;
            cout<< "160 S(3,2)= "<<(int) s[2][1]<<endl;
            cout<< "170 S(3,3)= "<<(int) s[2][2]<<endl;
            cout<< "180 S(3,4)= "<<(int) s[2][3]<<endl<<endl;

            cout<< "189 REM TYLNIA"<<endl;
            cout<< "190 S(4,1)= "<<(int) s[3][0]<<endl;
            cout<< "200 S(4,2)= "<<(int) s[3][1]<<endl;
            cout<< "210 S(4,3)= "<<(int) s[3][2]<<endl;
            cout<< "220 S(4,4)= "<<(int) s[3][3]<<endl<<endl;

            cout<< "229 REM LEWA"<<endl;
            cout<< "230 S(5,1)= "<<(int) s[4][0]<<endl;
            cout<< "240 S(5,2)= "<<(int) s[4][1]<<endl;
            cout<< "250 S(5,3)= "<<(int) s[4][2]<<endl;
            cout<< "260 S(5,4)= "<<(int) s[4][3]<<endl<<endl;

            cout<< "269 REM PRAWA"<<endl;
            cout<< "270 S(6,1)= "<<(int) s[5][0]<<endl;
            cout<< "280 S(6,2)= "<<(int) s[5][1]<<endl;
            cout<< "290 S(6,3)= "<<(int) s[5][2]<<endl;
            cout<< "300 S(6,4)= "<<(int) s[5][3]<<endl<<endl;

        default:
            break;
        }
        drawCube();
    }
    return 0;
}
